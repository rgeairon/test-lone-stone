import React, { Component } from 'react';
import './App.css';

const choices = ['Pierre', 'Papier', 'Ciseaux']

class App extends Component {
  state = {
    userChoice: null,
    aiChoice: null,
    userScore: 0,
    aiScore: 0,
    lastResult: '',
  }

  play = (userChoice) => {
    const aiChoice = choices[Math.floor(Math.random() * Math.floor(3))]
    let lastResult = 'Egalité'
    let userScore = this.state.userScore
    let aiScore = this.state.aiScore
    if (aiChoice !== userChoice) {
      switch (userChoice) {
        case 'Pierre':
          if (aiChoice !== 'Papier') {
            lastResult = 'Gagné'
            userScore = userScore + 1
          } else {
            lastResult = 'Perdu'
            aiScore = aiScore + 1
          }
          break
        case 'Papier':
        if (aiChoice !== 'Ciseaux') {
          lastResult = 'Gagné'
          userScore = userScore + 1
        } else {
          lastResult = 'Perdu'
          aiScore = aiScore + 1
        }
          break
        case 'Ciseaux':
          if (aiChoice !== 'Pierre') {
            lastResult = 'Gagné'
            userScore = userScore + 1
          } else {
            lastResult = 'Perdu'
            aiScore = aiScore + 1
          }
          break
        default: break
      }
    } else {
      lastResult = 'Egalité'
    }
     
    this.setState({ 
      aiChoice,
      userChoice,
      lastResult,
      userScore,
      aiScore,
    })
  }

  render() {
    return (
      <div className="App">
        <div className='title'>
          <h1>Shifumi</h1>
        </div>
        <div className='body'>
          <div className='game'>
            <div className='buttons'>
              {choices.map(choice => <button onClick={() => this.play(choice)}>{choice}</button>)}
            </div>
            <div className='result'>{this.state.lastResult}</div>
          </div>
          <div className='results'>
            <div className='player'>
              <span>Player : </span>
              <span>{this.state.userScore}</span>
            </div>
            <div className='ai'>
              <span>Computer : </span>
              <span>{this.state.aiScore}</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
